import 'dart:convert';

import 'package:commit360/repository/localToken.dart';
import 'package:commit360/repository/localUserData.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqflite.dart';

import 'connectLocalDB.dart';

class CommitRepository {

  create(data) async{
    var token = await LocalToken().getToken();

    http.Response result = await http.post('http://360.qlt.kz/api/review/add',
        headers: {
          "Accept": "application/json",
          "Authorization" : 'Bearer $token'
        },
        body: data
    );

    print(result.body);

    return jsonDecode(result.body);
  }


  update(id, data) async{
    var token = await LocalToken().getToken();

    http.Response result = await http.post('http://360.qlt.kz/api/review/edit/$id',
        headers: {
          "Accept": "application/json",
          "Authorization" : 'Bearer $token'
        },
        body: data
    );

    return jsonDecode(result.body);
  }

  setCommitPhone(number) async{
    String listResult = '';

    Database db = await ConnectLocalDB().database;
    List listNumbers = await LocalUser().getUserCommitPhone();

    listNumbers[0] == '' ?
      listResult = number.toString() :
      listResult = '${listNumbers.join(',')}, ${number.toString()}';

    if(listNumbers.join(',').indexOf(number) == -1) await db.update('UserData', {'list_numbers': listResult} , where: 'id = ?', whereArgs: [1]);
  }

  setAllCommitPhone(data) async{
    Database db = await ConnectLocalDB().database;

    await db.update('UserData', {'list_numbers': data} , where: 'id = ?', whereArgs: [1]);
  }

  getCommitAll() async{
    var token = await LocalToken().getToken();

    http.Response get = await http.get('http://360.qlt.kz/api/review/all',
      headers: {
        "Accept": "application/json",
        "Authorization" : 'Bearer $token'
      },);
    
    var data = await jsonDecode(get.body);

    return data['data'];
  }

  getListCommitByNumber(number) async{
    var token = await LocalToken().getToken();

    http.Response get = await http.post('http://360.qlt.kz/api/review/find-by-phone',
      headers: {
        "Accept": "application/json",
        "Authorization" : 'Bearer $token'
      },
      body: {'phone': number}
    );

    return jsonDecode(get.body);
  }

  geCommitByNumber(number) async{
    var token = await LocalToken().getToken();

    http.Response get = await http.get('http://360.qlt.kz/api/review/show/$number',
      headers: {
        "Accept": "application/json",
        "Authorization" : 'Bearer $token'
      },
    );

    return jsonDecode(get.body);
  }

  getNumberHistory() async{
    var token = await LocalToken().getToken();

    http.Response get = await http.get('http://360.qlt.kz/api/review/all-request',
      headers: {
        "Accept": "application/json",
        "Authorization" : 'Bearer $token'
      },
    );

    return jsonDecode(get.body);
  }

  deleteCommit(id) async{
    var token = await LocalToken().getToken();

    http.Response get = await http.delete('http://360.qlt.kz/api/review/delete/$id',
        headers: {
          "Accept": "application/json",
          "Authorization" : 'Bearer $token'
        },
    );

    return jsonDecode(get.body);
  }
}