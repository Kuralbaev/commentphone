import 'package:sqflite/sqflite.dart';

import 'connectLocalDB.dart';

class ContactsRepository {
  get() async{
    Database db = await ConnectLocalDB().database;
    var list = await db.rawQuery('SELECT * FROM Contacts');

    return list ?? [];
  }

  getName(number) async{
    Database db = await ConnectLocalDB().database;
    var name = await db.query('Contacts',
        columns: ['name'],
        where: 'phone = ?',
        whereArgs: [number]);


    return  name.length == 0 ? {'name': 'No name'} : name[0];
  }

  set(name, phone) async{
    Database db = await ConnectLocalDB().database;
    await db.insert('Contacts', {'name': name, 'phone': phone});
  }
}