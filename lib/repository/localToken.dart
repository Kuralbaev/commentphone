import 'package:commit360/repository/connectLocalDB.dart';
import 'package:sqflite/sqflite.dart';

class LocalToken{
  setToken(token) async{
    Database db = await ConnectLocalDB().database;
    await db.update('Token', {'token': token} , where: 'id = ?', whereArgs: [1]);
  }

  getToken() async{
    Database db = await ConnectLocalDB().database;
    var list = await db.rawQuery('SELECT * FROM Token');

    return list == null ? '' : list[0]['token'] ?? '';
  }

  getStatusApp() async{
    Database db = await ConnectLocalDB().database;
    var list = await db.rawQuery('SELECT * FROM Token');

    return list[0]['setupApp'];
  }

  setStatusApp() async{
    Database db = await ConnectLocalDB().database;
    await db.update('Token', {'setupApp': '1'} , where: 'id = ?', whereArgs: [1]);
  }

  restartStatusApp() async{
    Database db = await ConnectLocalDB().database;
    await db.update('Token', {'setupApp': '0'} , where: 'id = ?', whereArgs: [1]);
  }
}