import 'dart:convert';

import 'package:http/http.dart' as http;

class Login{
  Future serverSignIn(data) async{
    http.Response result = await http.post('http://360.qlt.kz/api/login',
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data)
    );

    return jsonDecode(result.body);  
  }

  Future serverSignUp(data) async{
    http.Response result = await http.post('http://360.qlt.kz/api/register',
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data)
    );

    print(result);

    return jsonDecode(result.body);
  }
}