import 'dart:convert';
import 'package:commit360/repository/connectLocalDB.dart';
import 'package:commit360/services/UserService.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;
import 'localToken.dart';

class LocalUser{
  setUser(UserService user) async{
    Database db = await ConnectLocalDB().database;

    await db.update('UserData', user.toMap() , where: 'id = ?', whereArgs: [1]);
  }

  getUser() async{
    Database db = await ConnectLocalDB().database;
    var list = await db.rawQuery('SELECT * FROM UserData');

    return list[0];
  }

  getUserOfServer() async {
    var token = await LocalToken().getToken();
    http.Response get = await http.get('http://360.qlt.kz/api/user',
      headers: {
        "Accept": "application/json",
        "Authorization" : 'Bearer $token'
      },);

    var user = jsonDecode(get.body);

    return user['user'];
  }

  updateUserOnServer(data) async {
    var token = await LocalToken().getToken();
    http.Response get = await http.post('http://360.qlt.kz/api/profile-update',
      headers: {
        "Accept": "application/json",
        "Authorization" : 'Bearer $token'
      },
      body: data
    );

    var user = jsonDecode(get.body);

    print(user);

    return user;
  }

  getUserCommitPhone() async{
    var data = await getUser();

    if(data['list_numbers'].length == '') return [];
    if(data['list_numbers'].indexOf(',') == -1) return [data['list_numbers']];
    if(data['list_numbers'].indexOf(',') > -1) return data['list_numbers'].split(',');
  }

  getUserBalance() async{
    var data = await getUser();

    return data['total_token'];
  }

  setUserBalance() async{
    Database db = await ConnectLocalDB().database;
    int balance = await getUserBalance();

    await db.update('UserData', {'total_token': ++balance} , where: 'id = ?', whereArgs: [1]);
  }

  popUserBalance() async{
    Database db = await ConnectLocalDB().database;
    int balance = await getUserBalance();

    await db.update('UserData', {'total_token': balance - 3} , where: 'id = ?', whereArgs: [1]);
  }
}