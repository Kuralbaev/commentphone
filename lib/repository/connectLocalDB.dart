import 'dart:io';

import 'package:commit360/services/UserService.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class ConnectLocalDB{
  static final _databaseName = "my_db.db";
  static final _databaseVersion = 1;

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await _initDatabase();
    return _database;
  }


  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    UserService user = new UserService();

    await db.execute('CREATE TABLE UserData (id INTEGER PRIMARY KEY,name TEXT, email TEXT, login TEXT,phone TEXT,list_numbers TEXT,total_token INTEGER,balance INTEGER,api_token TEXT,used_token INTEGER)');
    await db.execute('CREATE TABLE Token (id INTEGER PRIMARY KEY,token TEXT, setupApp TEXT)');
    await db.execute('CREATE TABLE Contacts (id INTEGER PRIMARY KEY,name TEXT, phone TEXT)');

    await db.insert('Token', {'id': 1, 'token': '', 'setupApp': '0'});
    await db.insert('UserData', user.toMap());
    await db.insert('Contacts', {'name': '', 'phone': ''});
  }
}