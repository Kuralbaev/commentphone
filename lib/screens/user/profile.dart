import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/screens/list/myComments.dart';
import 'package:commit360/screens/sign/in.dart';
import 'package:commit360/screens/user/resetPassword.dart';
import 'package:commit360/screens/user/updateUser.dart';
import 'package:commit360/services/UserService.dart';
import 'package:commit360/utilites/styleCards.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget{

  @override
  _ProfilePage createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage>{
  var user = {};
  bool reload = true;
  var _balance = 0;

  getDataUser() async{
    await UserService().setUserByToken();
    var result = await LocalUser().getUser();
    var balance = await LocalUser().getUserBalance();

    if(reload) if (mounted) setState(() {
      user = result;
      _balance = balance;
    });

    reload = !reload;
  }

  _info(){
    return Container(
      decoration: cardStyle(),
      padding: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(width: 250,
                child: Text(user['name'] ?? 'No Name', style: labelGrey(24), overflow: TextOverflow.ellipsis,),),
                IconButton(
                  onPressed: (){
                    Navigator.pushReplacement(context, PageRouteBuilder(pageBuilder: (context, _, __) => UpdateUserPage()));
                  },
                  padding: EdgeInsets.all(0),
                  icon: Icon(Icons.edit),
                )
              ],
            ),
          ),
          SizedBox(height: 10,),
          Container(
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(width: 1, color: Color.fromRGBO(248, 248, 248, 1)))
            ),
          ),

          SizedBox(height: 10),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Сотовый:'),
              Text(user['phone'] ?? '...'),
            ],
          ),

          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Оставленные отзывы:'),
              Text(user['commitPhone'].toString().split(',').length.toString()),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getDataUser();

    return Scaffold(
      appBar: AppBarWidget().app(back: true),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Мой профиль', style: labelGrey(16),),
                Text('Баланс: $_balance', style: labelGrey(16),)
              ],
            ),
            SizedBox(height: 30,),

            _info(),
            SizedBox(height: 30,),

            ButtonWidget().white(handlerCommit, text:'Мои отзывы'),
            SizedBox(height: 10,),
            ButtonWidget().white(handlerResetPassword, text:'Сменить пароль'),
            SizedBox(height: 10,),
            ButtonWidget().white(logout, text:'Выйти')
          ],
        ),
      ),
      bottomSheet: Container(
        width: double.infinity,
        padding: EdgeInsets.all(15),
        child: MaterialButton(
          onPressed: (){
            launch('https://qlt.kz');
          },
          child: Text('Политика конфиденциальности', style: labelButtonWhiteOrange(), textAlign: TextAlign.center,),
        ),
      ),
    );
  }

  handlerCommit(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ListMyCommentsPage()));
  }

  handlerResetPassword(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPassword()));
  }

  logout() async{
    await UserService().setLogout();
    Navigator.pushReplacement(context, PageRouteBuilder(pageBuilder: (context, _, __) => SignIn()));
  }
}