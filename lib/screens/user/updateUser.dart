import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/screens/user/profile.dart';
import 'package:commit360/services/UserService.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UpdateUserPage extends StatefulWidget{

  @override
  _UpdateUserPage createState() => _UpdateUserPage();
}

class _UpdateUserPage extends State<UpdateUserPage>{
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final numberController = TextEditingController();
  var dataLocalUser;
  bool _buttonLoading = false;

  getData() async{
    dataLocalUser = await LocalUser().getUser();
  }


  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    emailController.dispose();
    numberController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    getData();

    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 249, 252, 1),
      appBar: AppBarWidget().app(back: true),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(35),
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Редактировать', style: labelBlue(24)),
            SizedBox(height: 15),
            InputWidget().input(nameController, placeholder: 'Имя'),
            SizedBox(height: 15),

            InputWidget().input(emailController, placeholder: 'E-mail'),
            SizedBox(height: 15),

            InputWidget().input(numberController, placeholder: 'Номер', phone: true),

            SizedBox(height: 15),
            ButtonWidget().blue(handlerClick, text: 'Сохранить', load: _buttonLoading),

            SizedBox(height: 105),
          ],
        ),
      ),
    );
  }

  handlerClick() async{
    if (mounted) setState(() {
      _buttonLoading = true;
    });

    var data = {
      "name": nameController.text == '' ? dataLocalUser['name'] : nameController.text,
      "email": emailController.text == '' ? dataLocalUser['email'] ?? '' : emailController.text,
      "phone": numberController.text == '' ? dataLocalUser['phone'] : numberController.text,
    };

    print(data);

    await UserService().updateUser(data);
    await UserService().setUserByToken();

    Navigator.of(context).pop();
    Navigator.push(context, PageRouteBuilder(pageBuilder: (context, _, __) => ProfilePage()));
  }
}