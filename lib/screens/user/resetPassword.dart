import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ResetPassword extends StatefulWidget{

  @override
  _ResetPassword createState() => _ResetPassword();
}

class _ResetPassword extends State<ResetPassword>{
  final oldPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    oldPasswordController.dispose();
    newPasswordController.dispose();
    confirmPasswordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 249, 252, 1),
      appBar: AppBarWidget().app(back: true),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(35),
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Смена пароля', style: labelBlue(24)),
            SizedBox(height: 15),
            InputWidget().input(oldPasswordController, placeholder: 'Действующий пароль', password: true),
            SizedBox(height: 15),

            InputWidget().input(newPasswordController, placeholder: 'Новый пароль', password: true),
            SizedBox(height: 15),

            InputWidget().input(confirmPasswordController, placeholder: 'Подтвердите пароль', password: true),

            SizedBox(height: 15),
            ButtonWidget().blue(handlerClick, text: 'Сохранить'),

            SizedBox(height: 105),
          ],
        ),
      ),
    );
  }

  handlerClick() async{

  }
}