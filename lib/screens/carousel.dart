import 'package:carousel_slider/carousel_slider.dart';
import 'package:commit360/repository/localToken.dart';
import 'package:commit360/screens/list/historySearchNumber.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'main.dart';

class Carousel extends StatefulWidget {
  @override
  _Carousel createState() => _Carousel();
}

class _Carousel extends State<Carousel>{
  CarouselController buttonCarouselController = CarouselController();
  int indexSlider = 1;

  _dotSlider() => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Container(
        width: 10,
        height: 10,
        margin: EdgeInsets.all(3),
        decoration: BoxDecoration(
            color: indexSlider == 1 ? Color.fromRGBO(91, 92, 95, 1) : Color.fromRGBO(218, 223, 232, 1),
            borderRadius: BorderRadius.circular(30)
        ),
      ),
      Container(
        width: 10,
        height: 10,
        margin: EdgeInsets.all(3),
        decoration: BoxDecoration(
            color: indexSlider == 2 ? Color.fromRGBO(91, 92, 95, 1) : Color.fromRGBO(218, 223, 232, 1),
            borderRadius: BorderRadius.circular(30)
        ),
      ),
      Container(
        width: 10,
        height: 10,
        margin: EdgeInsets.all(3),
        decoration: BoxDecoration(
            color: indexSlider == 3 ? Color.fromRGBO(91, 92, 95, 1) : Color.fromRGBO(218, 223, 232, 1),
            borderRadius: BorderRadius.circular(30)
        ),
      ),
    ],
  );

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 249, 252, 1),
      appBar: AppBarWidget().app(back: true),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(30),
              child: Text('Как это работает?', style: labelBlue(20)),
            ),
            SizedBox(height: 30),
            Stack(
              children: <Widget>[
                CarouselSlider(
                    carouselController: buttonCarouselController,
                    options: CarouselOptions(
                        height: 400,
                        viewportFraction: 1.05,
                        onPageChanged: (index, int){
                          if (mounted) setState(() {
                            indexSlider = (index + 1);
                          });
                        }
                    ),
                    items: [
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/images/slider_1.png'),
                                fit: BoxFit.cover
                            )
                        ),
                        width: double.infinity,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/images/slider_2.png'),
                                fit: BoxFit.cover
                            )
                        ),
                        width: double.infinity,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/images/slider_3.png'),
                                fit: BoxFit.contain,
                            )
                        ),
                        width: double.infinity,
                      ),

                    ]
                ),

                Positioned(
                  right: -15,
                  top: 180,
                  child: MaterialButton(
                    onPressed: (){
                      if (mounted) setState(() {
                        buttonCarouselController.nextPage();
                        indexSlider += 1;
                      });
                    },
                    padding: EdgeInsets.all(0),
                    child: indexSlider == 3 ? Text('') : Icon(Icons.chevron_right, size: 100, color: Color.fromRGBO(134, 135, 149, 0.3),),
                  ),
                )
              ],
            ),

            SizedBox(height: 30,),
            _dotSlider()
          ],
        ),
      ),

      bottomSheet: Container(
        padding: EdgeInsets.all(30),
        child: indexSlider == 3 ? ButtonWidget().blue(handleClick ,text: 'Начать') : Text(''),
      ),
    );
  }

  handleClick() async{
     await LocalToken().setStatusApp();

     var status = await LocalToken().getStatusApp();

     status == '0' ?
        Navigator.push(context, PageRouteBuilder(pageBuilder: (context, _, __) => MainPage())) :
        Navigator.push(context, PageRouteBuilder(pageBuilder: (context, _, __) => ListHistorySearchPhonePage()));

  }
}