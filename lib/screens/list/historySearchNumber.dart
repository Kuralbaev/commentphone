import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/screens/commit/viewByPhone.dart';
import 'package:commit360/services/CommitService.dart';
import 'package:commit360/services/ContacsFromPhone.dart';
import 'package:commit360/services/UserService.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleCards.dart';
import 'package:commit360/widget/alert.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/input.dart';
import 'package:commit360/widget/card.dart';
import 'package:commit360/widget/loader.dart';
import 'package:commit360/widget/navigationBar.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';

class ListHistorySearchPhonePage extends StatefulWidget {
  @override
  _ListHistorySearchPhonePage createState() => _ListHistorySearchPhonePage();
}

class _ListHistorySearchPhonePage extends State<ListHistorySearchPhonePage>{

  var searchController = new TextEditingController();
  List _listContacts;
  List sentCommitPhones = [];
  String searchText = '';

  int _balance = 0;
  bool reload = true;

  getNumber() async{
    _listContacts = await CommitService().getHistoryNumber();
    var balance = await LocalUser().getUserBalance();

    await UserService().setUserByToken();
    await ContactsFromPhoneService().set();

    if(reload){
      if (mounted) setState(() {
        _balance = balance;
        _listContacts = _listContacts;
      });
    }

    reload = false;
  }


  @override
  void dispose() {
    super.dispose();

    searchController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    getNumber();

    return Scaffold(
      appBar: AppBarWidget().app(user: true),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Text('Баланс: $_balance', style: TextStyle(color: Color.fromRGBO(134, 135, 149, 1)), textAlign: TextAlign.end,),
            SizedBox(height: 10),
            Container(
              height: height - 230,
              padding: EdgeInsets.all(15),
              width: double.infinity,
              decoration: cardStyle(),
              child: Column(
                children: <Widget>[
                  Text('Посмотреть отзывы по номеру'),
                  SizedBox(height: 5),
                  InputWidget().search(searchController , handlerSearch, 'Поиск контакта'),

                  SizedBox(height: 15),

                  Container(
                    width: double.infinity,
                    height: height - 350,
                    child: _listContacts == null ? LoaderWidget().list('Секундочку, получаем ваши данные') : ListView(
                      children: <Widget>[
                        for(var contact in _listContacts)
                          contact['name'].toLowerCase().indexOf(searchText.toLowerCase()) > -1 ?
                          CardWidget().byPhone(
                              contact,
                              handlerAdd,
                              sentCommitPhones.join(',').indexOf(contact['phone']
                              )
                          ): Container()
                      ],
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: NavigatorBar(3),
    );
  }


  handlerSearch() async{
    if (mounted) setState(() {
      searchText = searchController.text;
    });
  }

  handlerAdd(name, number){
    Navigator.push(context, MaterialPageRoute(builder: (context) => ViewCommitByPhone(name, number)));
  }

  handlerContinue(){
    Navigator.push(context, FadeRoute(page: ListHistorySearchPhonePage()));
  }
}