import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/screens/list/historySearchNumber.dart';
import 'package:commit360/services/ContacsFromPhone.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleCards.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:commit360/widget/card.dart';
import 'package:commit360/widget/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../commit/create.dart';

class ReplenishBalance extends StatefulWidget{
  @override
  _ReplenishBalance createState() => _ReplenishBalance();
}

class _ReplenishBalance extends State<ReplenishBalance>{
  var searchController = new TextEditingController();
  List _listContacts = [];
  List sentCommitPhones = [];
  String searchText = '';

  bool reload = true;
  int _balance = 0;

  getNumber() async{
    _listContacts = await ContactsFromPhoneService().get();
    sentCommitPhones = await LocalUser().getUserCommitPhone();
    var balance = await LocalUser().getUserBalance();

    if (mounted) setState(() {
      _balance = balance;
      _listContacts = _listContacts;
    });
  }

  @override
  void dispose() {
    super.dispose();

    searchController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    getNumber();

    return Scaffold(
      appBar: AppBarWidget().app(),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Text('Оставлено $_balance из 3', style: labelBlue(16), textAlign: TextAlign.end,),
            SizedBox(height: 10),
            Container(
              height: height - 190,
              padding: EdgeInsets.all(15),
              width: double.infinity,
              decoration: cardStyle(),
              child: Column(
                children: <Widget>[
                  InputWidget().search(searchController , handlerSearch, 'Поиск контакта'),

                  SizedBox(height: 15),

                  Container(
                    width: double.infinity,
                    height: height - 290,
                    child: _listContacts.length == 0 ? LoaderWidget().list('Секундочку, получаем ваши контакты') :
                    ListView(
                      children: <Widget>[
                        for(var contact in _listContacts)
                          contact['name'].toLowerCase().indexOf(searchText.toLowerCase()) > -1 ?
                            CardWidget().add(
                                contact,
                                handlerAdd,
                                sentCommitPhones.join(',').indexOf(contact['phone'])
                            ) : Container()
                      ],
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),

      bottomSheet: Container(
        color: Color.fromRGBO(0, 0, 0, 0),
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
        child: ButtonWidget().blue(handlerContinue, text: 'Продолжить', disabled: _balance < 3),
      ),
    );
  }

  handlerSearch() async{
    if (mounted) setState(() {
      searchText = searchController.text;
    });
  }

  handlerAdd(name, number){
    Navigator.push(context, MaterialPageRoute(builder: (context) => CreateCommitPage(name, number)));
  }

  handlerContinue(){
    Navigator.push(context, FadeRoute(page: ListHistorySearchPhonePage()));
  }
}