import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/screens/list/historySearchNumber.dart';
import 'package:commit360/services/UserService.dart';
import 'package:commit360/services/ContacsFromPhone.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleCards.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/input.dart';
import 'package:commit360/widget/card.dart';
import 'package:commit360/widget/loader.dart';
import 'package:commit360/widget/navigationBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../commit/create.dart';

class ListAddNewCommit extends StatefulWidget{
  @override
  _ListContacts createState() => _ListContacts();
}

class _ListContacts extends State<ListAddNewCommit> with SingleTickerProviderStateMixin{
  var searchController = new TextEditingController();
  TabController _controller;

  List _listContacts;
  List sentCommitPhones = [];
  String searchText = '';

  int tabsCount = 0;
  int _balance = 0;
  bool reload = true;

  getData() async {
    _listContacts = await ContactsFromPhoneService().get();
    sentCommitPhones = await LocalUser().getUserCommitPhone();
    var balance = await LocalUser().getUserBalance();

    if (mounted) setState(() {
      _balance = balance;
      _listContacts = _listContacts;
    });

    reload = false;
  }


  _tabWidget(){
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color.fromRGBO(218, 223, 232, 1)))
      ),
      height: 20,
      child: new TabBar(
        indicatorWeight: 1,
        indicatorColor: Color.fromRGBO(235, 137, 0, 1),
        controller: _controller,
        isScrollable: true,
        labelStyle: labelOrange(14),
        labelColor: Color.fromRGBO(235, 137, 0, 1),
        unselectedLabelColor: Color.fromRGBO(134, 135, 149, 1),
        onTap: (index){
          if (mounted) setState(() {
            tabsCount = index;
          });
        },
        tabs: [
          new Tab(
            text: 'Все',
          ),
          new Tab(
            text: 'Отзыв оставлен',
          ),
          new Tab(
            text: 'Отзыв не оставлен',
          ),
        ],
      ),
    );
  }

  _card(contact){
    if(tabsCount == 0 && contact['name'].toLowerCase().indexOf(searchController.text.toLowerCase()) > -1) return CardWidget().add(
      contact,
      handlerAdd,
      sentCommitPhones.join(',').indexOf(contact['phone']),
    );
    else if(tabsCount == 1 && contact['name'].toLowerCase().indexOf(searchController.text.toLowerCase()) > -1 && sentCommitPhones.join(',').indexOf(contact['phone']) > -1) return CardWidget().add(
      contact,
      handlerAdd,
      sentCommitPhones.join(',').indexOf(contact['phone']),
    );
    else if(tabsCount == 2 && contact['name'].toLowerCase().indexOf(searchController.text.toLowerCase()) > -1 && sentCommitPhones.join(',').indexOf(contact['phone']) == -1) return CardWidget().add(
      contact,
      handlerAdd,
      sentCommitPhones.join(',').indexOf(contact['phone']),
    );

    return Container();
  }

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 3, vsync: this);
  }



  @override
  void dispose() {
    super.dispose();

    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getData();
    final height = MediaQuery.of(context).size.height;


    return Scaffold(
      appBar: AppBarWidget().app(user: true),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Поиск отзывов по номеру', style: labelGrey(16),),
                Text('Баланс: $_balance', style: labelGrey(16),)
              ],
            ),
            SizedBox(height: 10),
            Container(
              height: height - 230,
              padding: EdgeInsets.all(15),
              width: double.infinity,
              decoration: cardStyle(),
              child: Column(
                children: <Widget>[
                  InputWidget().search(searchController , handlerSearch, 'Поиск контакта'),

                  SizedBox(height: 25),

                  _tabWidget(),

                  Container(
                    width: double.infinity,
                    height: height - 355,
                    child:
                    _listContacts == null ? LoaderWidget().list('Секундочку, получаем ваши контакты') :
                    ListView(
                      children: <Widget>[
                        for(var contact in _listContacts) _card(contact)
                      ],
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),

      bottomNavigationBar: NavigatorBar(1),
    );
  }

  handlerSearch() async{
    if (mounted) setState(() {
      searchText = searchController.text;
    });
  }

  handlerAdd(name, number){
    Navigator.push(context, MaterialPageRoute(builder: (context) => CreateCommitPage(name, number)));
  }

  handlerContinue(){
    Navigator.push(context, FadeRoute(page: ListHistorySearchPhonePage()));
  }
}