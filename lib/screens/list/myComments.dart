import 'dart:async';

import 'package:commit360/screens/commit/update.dart';
import 'package:commit360/services/CommitService.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/alert.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/card.dart';
import 'package:commit360/widget/navigationBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListMyCommentsPage extends StatefulWidget{
  @override
  _ListMyCommentsPage createState() => _ListMyCommentsPage();
}

class _ListMyCommentsPage extends State<ListMyCommentsPage>{
  var _listComments = [];

  bool reload = true;

  getComments() async{
    var result = await CommitService().getAll();

    if(reload) if (mounted) setState(() {
      _listComments = result;
    });

    Timer.periodic(new Duration(seconds: 3), (timer) {
      reload = true;
    });

    reload = false;
  }



  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getComments();

    return Scaffold(
      appBar: AppBarWidget().app(back: true),
      body: Container(
        padding: EdgeInsets.all(15),
        child: ListView(
          children: <Widget>[
            Text('Мои отзывы', style: labelGrey(16)),
            SizedBox(height: 15),

            _listComments?.length != 0 ? Column(
              children: <Widget>[
                for(var comment in _listComments) CardWidget().myCommit(comment, handlerUpdate, handlerRemove),
              ],
            ) : Container()
          ],
        ),
      ),
      bottomNavigationBar: NavigatorBar(3),
    );
  }

  handlerUpdate(id, name, number){
    Navigator.push(context, MaterialPageRoute(builder: (context) => UpdateCommitPage(id, name, number)));
  }

  handlerRemove(number) async{
    var result = await CommitService().delete(number);
    getComments();

    if(result['success']) AlertWidget().show(context, 'Комментарий успешно удалена', 'success');
    else AlertWidget().show(context, 'Возникли проблемы', 'error');
  }
}