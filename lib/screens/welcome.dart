import 'dart:async';

import 'package:commit360/repository/localToken.dart';
import 'package:commit360/screens/sign/in.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:flutter/material.dart';

import 'list/historySearchNumber.dart';

class Welcome extends StatefulWidget{
  @override
  _Welcome createState() => _Welcome();
}

class _Welcome extends State<Welcome>{
  bool _auth = true;

  _verifiedAuth() async{
      var token = await LocalToken().getToken();

      if(token == '' || token == null) _auth = false;

      print('Token: $token');

      Timer(Duration(seconds: 3), () {
        _auth ?
        RouterService().link(context , ListHistorySearchPhonePage()):
        RouterService().link(context, SignIn());
      });
  }

  next() async{
    await _verifiedAuth();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    next();

    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 249, 252, 1),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/welcome.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}