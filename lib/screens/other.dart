import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/navigationBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'carousel.dart';

class OtherPage extends StatefulWidget{
  @override
  _ListContacts createState() => _ListContacts();
}

class _ListContacts extends State<OtherPage>{
  int _balance = 0;
  bool reload = true;

  getData() async{
    var balance = await LocalUser().getUserBalance();

    if(reload) if (mounted) setState(() {
      _balance = balance;
    });

    reload = !reload;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getData();

    return Scaffold(
      appBar: AppBarWidget().app(user: true),
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Другое', style: labelGrey(16),),
                Text('Баланс: $_balance', style: labelGrey(16),)
              ],
            ),
            SizedBox(height: 200),
            ButtonWidget().white(_howWork, text: 'Как это работает?', textColor: 'orange'),
            SizedBox(height: 20),
            ButtonWidget().white(handlerClick, text: 'Политика конфиденциальности', textColor: 'orange'),
          ],
        ),
      ),

      bottomNavigationBar: NavigatorBar(2),
    );
  }

  _howWork(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => Carousel()));
  }

  handlerClick(){
    launch('https://qlt.kz');
  }
}