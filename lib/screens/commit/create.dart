import 'package:commit360/services/CommitService.dart';
import 'package:commit360/services/UserService.dart';
import 'package:commit360/utilites/styleCards.dart';
import 'package:commit360/widget/alert.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CreateCommitPage extends StatefulWidget{
  var name;
  var number;
  CreateCommitPage(this.name, this.number);

  @override
  _CreateCommitPage createState() => _CreateCommitPage();
}

class _CreateCommitPage extends State<CreateCommitPage>{
  var positiveCommitController = TextEditingController();
  var negativeCommitController = TextEditingController();
  var commitController = TextEditingController();

  bool _buttonLoading = false;

  @override
  void dispose() {
    super.dispose();

    positiveCommitController.dispose();
    negativeCommitController.dispose();
    commitController.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBarWidget().app(back: true),
        body: Container(
          padding: EdgeInsets.all(15),
          child: Container(
            padding: EdgeInsets.all(20),
            width: double.infinity,
            decoration: cardStyle(),
            child: ListView(
              children: <Widget>[
                Text(widget.name, style: TextStyle(
                    color: Color.fromRGBO(134, 135, 149, 1),
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                )),
                SizedBox(height: 5,),
                Text(widget.number, style: TextStyle(
                    color: Color.fromRGBO(134, 135, 149, 1),
                    fontSize: 14,
                    fontWeight: FontWeight.w300
                )),
                SizedBox(height: 20),

                InputWidget().textarea(positiveCommitController, label: 'Положительные качества'),
                SizedBox(height: 20),

                InputWidget().textarea(negativeCommitController, label: 'Отрицательные качества'),
                SizedBox(height: 20),

                InputWidget().textarea(commitController, label: 'Комментарий'),
                SizedBox(height: 20),

                ButtonWidget().blue(handlerClick, text: 'Отправить', load: _buttonLoading)
              ],
            ),
          ),
        )
    );
  }

  handlerClick() async{
    if (mounted) setState(() {
      _buttonLoading = true;
    });

    if(positiveCommitController.text == '' && negativeCommitController.text == '' && commitController.text == ''){
      AlertWidget().show(context, 'Один поле обязательный', 'error');
      return ;
    }

    var data = {
      'phone': widget.number,
      'good_review': positiveCommitController.text ?? '',
      'bad_review': negativeCommitController.text ?? '',
      'comment': commitController.text ?? '',
    };


    await CommitService().create(data);
    await UserService().setUserByToken();
    Navigator.of(context).pop();
  }

  alertShow(text){
    AlertWidget().show(context, text, 'error');
  }
}