import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/screens/commit/create.dart';
import 'package:commit360/services/CommitService.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ViewCommitByPhone extends StatefulWidget{
  String name;
  String phone;

  ViewCommitByPhone(this.name, this.phone);

  @override
  _ViewCommitByPhone createState() => _ViewCommitByPhone();
}

class _ViewCommitByPhone extends State<ViewCommitByPhone>{
  bool sentCommit = false;
  var _listComments = [];

  bool reload = true;

  getNumber() async{
    List listNumbers = await LocalUser().getUserCommitPhone();
    listNumbers.join(',').indexOf(widget.phone) == -1 ? sentCommit = false : sentCommit = true;

    await getCommit();

    if(reload) if (mounted) setState(() {
      sentCommit = sentCommit;
    });

    reload = false;
  }

  getCommit() async{
    var result = await CommitService().getListCommitByNumber(widget.phone);

    _listComments = result;
  }

  _buttonAdd(){
    return
      Container(
        width: 60,
        height: 30,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: sentCommit ? Color.fromRGBO(218, 223, 232, 1) : Color.fromRGBO(39, 174, 96, 1)),
          color: sentCommit ? Colors.white : Color.fromRGBO(39, 174, 96, 1),
        ),
        child: Center(
          child: MaterialButton(
            onPressed: (){
              if(!sentCommit)  Navigator.push(context, MaterialPageRoute(builder: (context) => CreateCommitPage(widget.name, widget.phone)));
            },
            padding: EdgeInsets.all(0),
            child:  sentCommit ? Icon(Icons.check, color: Color.fromRGBO(218, 223, 232, 1)) : Icon(Icons.add, color: Colors.white),
          ),
        ),
      );
  }



  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getNumber();

    return Scaffold(
      appBar: AppBarWidget().app(back: true),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Отзывы о ${widget.name == '' ? widget.phone : widget.name}', style: labelGrey(16),),
                _buttonAdd()
              ],
            ),
            SizedBox(height: 15,),

            for (var commit in _listComments) CardWidget().commit(commit)
          ],
        ),
      ),
    );
  }
}