import 'package:commit360/screens/list/myComments.dart';
import 'package:commit360/services/CommitService.dart';
import 'package:commit360/services/UserService.dart';
import 'package:commit360/utilites/styleCards.dart';
import 'package:commit360/widget/alert.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class UpdateCommitPage extends StatefulWidget{
  var id;
  var name;
  var number;
  UpdateCommitPage(
      this.id,
      this.name,
      this.number,
      );

  @override
  _UpdateCommitPage createState() => _UpdateCommitPage();
}

class _UpdateCommitPage extends State<UpdateCommitPage>{
  var positiveCommitController = TextEditingController();
  var negativeCommitController = TextEditingController();
  var commitController = TextEditingController();

  bool reload = true;
  bool _buttonLoading = false;

  getCommit(number) async{
    var result = await CommitService().getCommitByNumber(number.toString());

    if(reload) if (mounted) setState(() {
      positiveCommitController.text = result['good_review'];
      negativeCommitController.text = result['bad_review'];
      commitController.text = result['comment'];
    });

    reload = !reload;
  }

  @override
  Widget build(BuildContext context) {
    getCommit(widget.id);

    return Scaffold(
        appBar: AppBarWidget().app(back: true),
        body: Container(
          padding: EdgeInsets.all(15),
          child: Container(
            padding: EdgeInsets.all(20),
            width: double.infinity,
            decoration: cardStyle(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(widget.name, style: TextStyle(
                    color: Color.fromRGBO(134, 135, 149, 1),
                    fontSize: 18,
                    fontWeight: FontWeight.bold
                )),
                SizedBox(height: 5,),
                Text(widget.number, style: TextStyle(
                    color: Color.fromRGBO(134, 135, 149, 1),
                    fontSize: 14,
                    fontWeight: FontWeight.w300
                )),
                SizedBox(height: 20),

                InputWidget().textarea(positiveCommitController, label: 'Положительные качества'),
                SizedBox(height: 20),

                InputWidget().textarea(negativeCommitController, label: 'Отрицательные качества'),
                SizedBox(height: 20),

                InputWidget().textarea(commitController, label: 'Комментарий'),
                SizedBox(height: 20),

                ButtonWidget().blue(handlerClick, text: 'Обновить', load: _buttonLoading)
              ],
            ),
          ),
        )
    );
  }

  handlerClick() async{
    if (mounted) setState(() {
      _buttonLoading = true;
    });

    if(positiveCommitController.text == '' && negativeCommitController.text == '' && commitController.text == ''){
      alertShow('Один поле обязательный', 'error');
      return ;
    }

    var data = {
      'phone': widget.number,
      'good_review': positiveCommitController.text ?? '',
      'bad_review': negativeCommitController.text ?? '',
      'comment': commitController.text ?? '',
    };

    await CommitService().update(widget.id, data);
    await UserService().setUserByToken();

    Navigator.of(context).pop();
    Navigator.of(context).pop();
    Navigator.push(context, PageRouteBuilder(pageBuilder: (context, _, __) => ListMyCommentsPage()));
  }

  alertShow(text, type){
    AlertWidget().show(context, text, type);
  }
}