import 'package:commit360/services/login/signUp.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../carousel.dart';

class SignUp extends StatefulWidget{

  @override
  _SignUp createState() => _SignUp();
}

class _SignUp extends State<SignUp>{
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  bool checkedConf = false;

  Widget _checkBox(){
    return Container(
        width: double.infinity,
        child: Row(
          children: <Widget>[
            Checkbox(
              value: checkedConf,
              onChanged: _valueChanged,
              activeColor: Color.fromRGBO(0, 74, 162, 1),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Я согласен с '),
                Container(
                  height: 20,
                  child: MaterialButton(
                    height: 10,
                    padding: EdgeInsets.all(0),
                    onPressed: (){
                      launch('https://qlt.kz');
                    },
                    child: Text('политикой конфиденциальности', style: TextStyle(color: Color.fromRGBO(235, 138, 0, 1))),
                  ),
                )
              ],
            )
          ],
        )
    );
  }


  @override
  void dispose() {
    super.dispose();

    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 249, 252, 1),
      appBar: AppBarWidget().app(back: true),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(35),
        height: height,
        child: ListView(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Регистрация', style: labelBlue(24)),

                SizedBox(height: 30),
                InputWidget().input(nameController, label: 'Ваше имя'),

                SizedBox(height: 10),
                InputWidget().input(emailController, label: 'Ваш e-mail'),

                SizedBox(height: 10),
                InputWidget().input(phoneController, label: 'Номер телефона', phone: true),

                SizedBox(height: 10),
                InputWidget().input(passwordController, label: 'Придумайте пароль', password: true),

                SizedBox(height: 10),
                InputWidget().input(confirmPasswordController, label: 'Подтвердите пароль', password: true),

                SizedBox(height: 10),
                _checkBox(),

                SizedBox(height: 70),
                ButtonWidget().blue(handlerClick, text: 'Войти', disabled: !checkedConf),
              ],
            )
          ],
        ),
      ),
    );
  }

  handlerClick()async{
    if(nameController.text == '') alertShow('Заполните ваше имя');
    else if(emailController.text == '') alertShow('Заполните E-mail');
    else if(phoneController.text == '') alertShow('Заполните номер телефона');
    else if(passwordController.text == '') alertShow('Заполните пароль');
    else if(passwordController.text == '') alertShow('Пароль не совпадает');
    else SignUpServer(nameController.text, emailController.text, phoneController.text, passwordController.text).call()
          .then((value){
            if(!value) alertShow('Такой номер или email уже существует');
            else Navigator.push(context, FadeRoute(page: Carousel()));
          });
  }

  alertShow(text){
    Flushbar(
      message: text,
      backgroundColor: Color.fromRGBO(235, 87, 87, 1),
      duration:  Duration(seconds: 3),
    )..show(context);
  }

  void _valueChanged(bool value) => setState(() => checkedConf = value);
}