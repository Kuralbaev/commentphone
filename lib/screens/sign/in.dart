import 'dart:io';

import 'package:commit360/repository/localToken.dart';
import 'package:commit360/screens/carousel.dart';
import 'package:commit360/screens/main.dart';
import 'package:commit360/screens/sign/up.dart';
import 'package:commit360/services/ContacsFromPhone.dart';
import 'package:commit360/services/login/signIn.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/alert.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget{

  @override
  _SignIn createState() => _SignIn();
}

class _SignIn extends State<SignIn>{
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();

  bool errorForm = false;
  bool _connectRepeat = true;
  bool _buttonLoading = false;

  _connect()async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
      }
    } on SocketException catch (_) {
      if(_connectRepeat){
        _connectRepeat = !_connectRepeat;
        AlertWidget().show(context, 'У вас отсутствует  подключение к Интернету. Для просмотра данной страницы подключитесь к интернету и перезапустите Приложение.', 'error');
      }
    }
  }

  _linkRegistration() => Container(
    padding: EdgeInsets.only(bottom: 35),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('Нет аккаунта?', style: TextStyle(
            color: Color.fromRGBO(134, 135, 149, 1),
            fontSize: 18
        )),
        MaterialButton(
          padding: EdgeInsets.all(0),
          onPressed: (){signUp();},
          child: Text(' Зарегистрируйтесь', style: labelBlue(18)),
        )
      ],
    ),
  );


  @override
  void dispose() {
    super.dispose();
    phoneController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _connect();
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Color.fromRGBO(248, 249, 252, 1),
      appBar: AppBarWidget().app(),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(35),
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Авторизация', style: labelBlue(24)),

            SizedBox(height: 30),
            InputWidget().input(phoneController, placeholder: '+7 XXX XXX XXXX', phone: true, error: errorForm),

            SizedBox(height: 15),
            InputWidget().input(passwordController, placeholder: 'Пароль', error: errorForm, password: true),

            errorForm ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 15),
                Text('Неправильный номер / пароль', style: labelERROR(), textAlign: TextAlign.start),
                SizedBox(height: 25)
              ],
            ) : Text(''),

//            SizedBox(height: 15),
            ButtonWidget().blue(handlerClick, text: 'Войти', load: _buttonLoading),

            SizedBox(height: 105),
          ],
        ),
      ),
      bottomSheet: _linkRegistration(),
    );
  }

  handlerClick()async{
    if (mounted) setState(() {
      _buttonLoading = true;
    });

    var result = await SignInService(phoneController.text, passwordController.text).call();
    var status = await LocalToken().getStatusApp();

    if (mounted) setState(() {
      errorForm = !result;
      _buttonLoading = result;
    });

    await ContactsFromPhoneService().set();

    if(result) status == '0' ?
      RouterService().link(context , Carousel()) :
      RouterService().link(context , MainPage());
  }

  signUp(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));
  }
}