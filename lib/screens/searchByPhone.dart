import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:commit360/widget/alert.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:commit360/widget/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:native_contact_picker/native_contact_picker.dart';

import 'commit/viewByPhone.dart';
import 'list/replenishBalance.dart';

class SearchByNumberPage extends StatefulWidget{
  @override
  _SearchByNumberPage createState() => _SearchByNumberPage();
}

class _SearchByNumberPage extends State<SearchByNumberPage>{
  var phoneController = TextEditingController();
  int _balance = 0;
  bool reload = true;

  // ignore: missing_return
  Widget _dialogModal(){
    showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        backgroundColor: Color.fromRGBO(0, 0, 0, 0),
        contentPadding: EdgeInsets.all(0),
        elevation: 0,
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Container(
                child:Column(
                  children: <Widget>[
                    Text('Чтобы просмотреть отзывы о другом человеке, необходимо иметь 3 единицы баланса', style:  TextStyle(
                      color:  Colors.white,
                      fontSize: 24
                    ), textAlign: TextAlign.center,),
                    SizedBox(height: 20,),
                    Icon(Icons.help, size: 30, color: Color.fromRGBO(0, 74, 162, 1),),
                    SizedBox(height: 20,),

                    ButtonWidget().blue(handlerModalClick, text:'Пополнить баланс')

                  ],
                ),
              )
            ],
          ),
        ),
      );
    });
  }

  getData() async{
    var balance = 0;

    if (mounted) setState(() {
      _balance = balance;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    getData();

    return Scaffold(
      appBar: AppBarWidget().app(back: true, user: true),
      body: Container(
        padding: EdgeInsets.all(25),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Поиск отзывов по номеру', style: labelGrey(16),),
                Text('Баланс: $_balance', style: labelGrey(16),)
              ],
            ),
            SizedBox(height: 80),

            Text('Введите номер телефона человека, отзывы на которого вы хотели бы посмотреть', style: labelBlue(20), textAlign: TextAlign.center,),
            SizedBox(height: 40),

            InputWidget().phone(phoneController, '+7 XXX XXX XXXX', f: handlerOpenContacts),
            SizedBox(height: 15),

            ButtonWidget().blue(handlerClick, text: 'Продолжить')
          ],
        ),
      ),
    );
  }

  handlerClick() async{
    if(phoneController.text == '') AlertWidget().show(context, 'Заполните номер', 'error');
    else if(_balance == 0) _dialogModal();
    else {
      await LocalUser().popUserBalance();
      Navigator.push(context, MaterialPageRoute(builder: (context) => ViewCommitByPhone('', phoneController.text)));
    }
  }

  handlerOpenContacts() async{
    final NativeContactPicker _contactPicker = new NativeContactPicker();
    Contact contact = await _contactPicker.selectContact();

    if (mounted) setState(() {
      phoneController.text = contact.toString()?.split(':')[1]?.trim();
    });
  }

  handlerModalClick(){
    Navigator.push(context, FadeRoute(page: ReplenishBalance()));
  }
}