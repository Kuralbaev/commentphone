import 'package:commit360/screens/list/replenishBalance.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/widget/appBar.dart';
import 'package:commit360/widget/button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget{

  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage>{
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget().app(),
      body: Container(
        padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 50),
            Text('Чтобы просмотреть отзывы по номеру, необходимо оставить отзывы о 3 своих контактах', style: TextStyle(
              color: Color.fromRGBO(134, 135, 149, 1),
              fontSize: 18
            ), textAlign: TextAlign.center,),
            SizedBox(height: 150),

            Text('Выберите из списка контакт для отзыва', style: TextStyle(
                color: Color.fromRGBO(0, 74, 161, 1),
                fontSize: 24,
                fontWeight: FontWeight.w700
            ),textAlign: TextAlign.center),
            SizedBox(height: 10),

            Text('Отзывы оставляются анонимно', style: TextStyle(
                color: Color.fromRGBO(134, 135, 149, 1),
                fontSize: 18,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w300
            ), textAlign: TextAlign.center,),
          ],
        ),
      ),
      bottomSheet: Container(
        padding: EdgeInsets.all(30),
        child: ButtonWidget().blue(handlerClick, text: 'Оставить отзывы'),
      ),
    );
  }

  handlerClick(){
    Navigator.push(context, FadeRoute(page: ReplenishBalance()));
  }
}