import 'dart:convert';

import 'package:commit360/repository/Commit.dart';
import 'package:commit360/repository/localUserData.dart';
import 'package:commit360/services/ContacsFromPhone.dart';

class CommitService {
  CommitService({
    this.id,
    this.userId,
    this.phone,
    this.goodReview,
    this.badReview,
    this.comment,
  });

  int id;
  int userId;
  String phone;
  String goodReview;
  String badReview;
  String comment;

  factory CommitService.fromJson(String str) => CommitService.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CommitService.fromMap(Map<String, dynamic> json) => CommitService(
    id: json["id"],
    userId: json["user_id"],
    phone: json["phone"],
    goodReview: json["good_review"],
    badReview: json["bad_review"],
    comment: json["comment"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "user_id": userId,
    "phone": phone,
    "good_review": goodReview,
    "bad_review": badReview,
    "comment": comment,
  };

  create(data) async{
    await CommitRepository().create(data);
    await CommitRepository().setCommitPhone(data['phone']);
    await LocalUser().setUserBalance();

    return true;
  }

  getAll() async{
    var get = await CommitRepository().getCommitAll();

    get?.map((comment) async{
      print(comment);
      var name = await ContactsFromPhoneService().getName(comment['phone']);
      comment['name'] = name['name'];
    });

    return get ?? [];
  }

  getListCommitByNumber(number) async{
    var get = await CommitRepository().getListCommitByNumber(number);

    return get['data'] ?? [];
  }

  getCommitByNumber(number) async{
    var get = await CommitRepository().geCommitByNumber(number);

    return get['data'];
  }

  getHistoryNumber() async{
    List newList = [];
    var get = await CommitRepository().getNumberHistory();

    await get['data']?.forEach((e) async {
      var name = await await ContactsFromPhoneService().getName(e.toString());

      newList.add({
        "name": name['name'],
        "phone": e.toString()
      });
    });

    return newList;
  }

  update(id, data) async{
    var get = await CommitRepository().update(id, data);

    return get['success'];
  }

  delete(id) async{
    var result = await CommitRepository().deleteCommit(id);

    return result;
  }
}
