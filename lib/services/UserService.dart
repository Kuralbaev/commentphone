import 'dart:convert';

import 'package:commit360/repository/Commit.dart';
import 'package:commit360/repository/localUserData.dart';

class UserService {
  UserService({
    this.login,
    this.name,
    this.email,
    this.phone,
    this.totalToken,
    this.usedToken,
    this.balance,
    this.listNumber,
    this.apiToken,
  });

  String login;
  String name;
  String email;
  String phone;
  int totalToken;
  int usedToken;
  int balance;
  String listNumber;
  dynamic apiToken;

  factory UserService.fromJson(String str) => UserService.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UserService.fromMap(Map<String, dynamic> json) => UserService(
    login: json["login"],
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    totalToken: json["total_token"],
    usedToken: json["used_token"],
    balance: json["balance"],
    listNumber: json["list_numbers"],
    apiToken: json["api_token"],
  );

  Map<String, dynamic> toMap() => {
    "login": login,
    "name": name,
    "email": email,
    "phone": phone,
    "total_token": totalToken,
    "used_token": usedToken,
    "balance": balance,
    "list_numbers": listNumber,
    "api_token": apiToken,
  };

  setLocalUser(user) async{
    UserService newUser = new UserService();

    newUser.name = user['name'] ?? '';
    newUser.login = user['login'] ?? '';
    newUser.email = user['email'] ?? '';
    newUser.phone = user['phone'] ?? '';
    newUser.totalToken = user['total_token'] ?? '';
    newUser.usedToken = user['used_token'] ?? '';
    newUser.apiToken = user['api_token'] ?? 0;
    newUser.balance = 0;
    newUser.listNumber = '';

    await LocalUser().setUser(newUser);
  }

  updateUser(user) async{
    await LocalUser().updateUserOnServer(user);

    await setUserByToken();
  }

  setLogout(){
    UserService newUser = new UserService();

    newUser.name = '';
    newUser.email = '';
    newUser.login = '';
    newUser.phone = '';
    newUser.totalToken = 0;
    newUser.usedToken = 0;
    newUser.apiToken = 0;
    newUser.balance = 0;
    newUser.listNumber = '';

    LocalUser().setUser(newUser);
  }

  setUserByToken() async{
    List listNumberComments = [];
    List allCommentUser = await CommitRepository().getCommitAll();
    var user = await LocalUser().getUserOfServer();

    allCommentUser.forEach((element) {
      listNumberComments.add(element['phone']);
    });

    await setLocalUser(user);
    await CommitRepository().setAllCommitPhone(listNumberComments.join(','));
  }
}