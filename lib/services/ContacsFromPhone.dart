import 'package:commit360/repository/Contacts.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactsFromPhoneService {
  Future get() async {
    var get = await ContactsRepository().get();

    return get ?? [];
  }

  getName(number) async{
    var get = await ContactsRepository().getName(number);

    return get;
  }

  set() async{

    List contactList = await get();

    var result = await PermissionHandler().requestPermissions([PermissionGroup.contacts]);

    if (result[PermissionGroup.contacts] == PermissionStatus.granted) {
      Iterable<Contact> listContacts = await ContactsService.getContacts();

      listContacts?.forEach((contact) {
        if (contact.toMap()['phones'].length > 0 && contactList.where((element) => element['phone'] == contact.toMap()['phones'][0]['value']).length == 0) ContactsRepository().set(
            contact.displayName == '' || contact?.displayName == null ? 'Name' : contact.displayName,
            contact.toMap()['phones'][0]['value'] ?? '...'
        );
      });
    }
  }
}