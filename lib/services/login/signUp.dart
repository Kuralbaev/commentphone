import 'package:commit360/repository/Login.dart';
import 'package:commit360/repository/localToken.dart';

import '../UserService.dart';

class SignUpServer{
  String name;
  String email;
  String phone;
  String password;

  SignUpServer(this.name,this.email, this.phone, this.password);

  Future call() async{
    var data = {
      "login": phone,
      "email": email,
      "name": name,
      "phone": phone,
      "password": password,
    };

    var result = await Login().serverSignUp(data);

    if(result['success']){
      await LocalToken().setToken(result['token']);
      await UserService().setLocalUser(result['user']);
      await LocalToken().restartStatusApp();
    }

    return result['success'];
  }
}