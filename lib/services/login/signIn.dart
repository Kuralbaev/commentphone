import 'package:commit360/repository/Login.dart';
import 'package:commit360/repository/localToken.dart';
import 'package:commit360/services/UserService.dart';

class SignInService{
  String phone;
  String password;

  SignInService(this.phone, this.password);

  Future call() async{
    var data = {
      'login': phone,
      'password': password
    };

    print(data);

    var result = await Login().serverSignIn(data);

    print('Login result $result');

    if(result['success']){
      await setToken(result['token']);
      await UserService().setLocalUser(result['user']);
    }

    return result['success'];
  }

  setToken(token){
    LocalToken().setToken(token);
  }
}