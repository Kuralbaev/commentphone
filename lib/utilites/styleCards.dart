import 'package:flutter/material.dart';

final cardStyle = () => BoxDecoration(
  color: Colors.white,
  borderRadius: new BorderRadius.circular(10.0),
  boxShadow: [
    new BoxShadow(
      color: Color.fromRGBO(31, 32, 65, 0.05),
      offset: new Offset(10, 20),
      blurRadius: 10,
    )
  ],
);