import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final labelBlue = (double size) => TextStyle(
    color: Color.fromRGBO(0, 74, 161, 1),
    fontSize: size,
    fontWeight: FontWeight.w500
  );

final labelGrey = (double size) => TextStyle(
    color: Color.fromRGBO(134, 135, 149, 1),
    fontSize: size,
    fontWeight: FontWeight.w500
  );

final labelOrange = (double size) => TextStyle(
    color: Color.fromRGBO(235, 137, 0, 1),
    fontSize: size,
    fontWeight: FontWeight.w500
  );

final labelButtonBlue = () => TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.normal
);

final labelButtonWhite = () => TextStyle(
    color: Color.fromRGBO(134, 135, 149, 1),
    fontSize: 18,
    fontWeight: FontWeight.normal
);

final labelButtonWhiteOrange = () => TextStyle(
    color: Color.fromRGBO(235, 138, 0, 1),
    fontSize: 18,
    fontWeight: FontWeight.normal
);

final labelERROR = () => TextStyle(
    color: Color.fromRGBO(235, 87, 87, 1),
    fontSize: 14,
    fontWeight: FontWeight.normal
);

final labelInput = () => TextStyle(
    color: Color.fromRGBO(134, 135, 149, 1),
    fontSize: 16,
    fontWeight: FontWeight.normal
);