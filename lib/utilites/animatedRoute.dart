import 'package:flutter/material.dart';

class FadeRoute extends PageRouteBuilder {
  final page;

  FadeRoute({this.page}) : super(
    pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation,) => page,
    transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child,) => FadeTransition(
      opacity: animation,
      child: child,
    ),
  );
}

class RouterService{
  link(context, page){
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation1, animation2) => page,
      ),
    );
  }
}