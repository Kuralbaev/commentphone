import 'package:commit360/utilites/styleLabel.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

//var maskFormatter = new MaskTextInputFormatter(mask: '+# (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });

class InputWidget {
  var input = (controller, {label = '', placeholder = '', phone = false, password = false, error = false}) =>
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            label == '' ? Container() : Text(
              label, style: labelInput(), textAlign: TextAlign.start,),
            SizedBox(height: 5,),
            Container(
              padding: EdgeInsets.only(left: 25, right: 25),
              height: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(55),
                  border: Border.all(
                      color: error ? Color.fromRGBO(235, 87, 87, 1) : Color
                          .fromRGBO(218, 223, 232, 1), width: 1)
              ),
              child: TextField(
//                  inputFormatters: phone ? [maskFormatter] : [],
                  obscureText: password,
                  controller: controller,
                  keyboardType: phone ? TextInputType.phone : TextInputType.text,
                  style: TextStyle(
                      color: error ? Color.fromRGBO(235, 87, 87, 1) : Color
                          .fromRGBO(0, 74, 161, 1),
                      fontSize: 16
                  ),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    hintText: placeholder,
                    hintStyle: TextStyle(
                        color: Color.fromRGBO(134, 135, 149, 1),
                        fontSize: 16
                    ),
                    contentPadding: EdgeInsets.symmetric(
                        horizontal: 0, vertical: 0),
                  )
              ),
            )
          ]
      );

  var textarea = (controller, {label = '', placeholder = '', password = false, error = false}) =>
      Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            label == '' ? Container() : Text(
              label, style: labelInput(), textAlign: TextAlign.start,),
            SizedBox(height: 5,),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
              height: 120,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                      color: error ? Color.fromRGBO(235, 87, 87, 1) : Color
                          .fromRGBO(218, 223, 232, 1), width: 1)
              ),
              child: TextField(
                  controller: controller,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  style: TextStyle(
                      color: error ? Color.fromRGBO(235, 87, 87, 1) : Color
                          .fromRGBO(0, 74, 161, 1),
                      fontSize: 16
                  ),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    hintText: placeholder,
                    hintStyle: TextStyle(
                        color: Color.fromRGBO(134, 135, 149, 1),
                        fontSize: 16
                    ),
                    contentPadding: EdgeInsets.symmetric(
                        horizontal: 0, vertical: 0),
                  )
              ),
            )
          ]
      );

  var search = (controller, function, placeholder) =>
      Container(
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(55),
            border: Border.all(
                color: Color.fromRGBO(218, 223, 232, 1), width: 1)
        ),
        child: TextField(
            controller: controller,
            onChanged: (str){
              function();
            },
            style: TextStyle(
                color: Color.fromRGBO(0, 74, 161, 1),
                fontSize: 16
            ),
            decoration: new InputDecoration(
              border: InputBorder.none,
              hintText: placeholder,
              prefixIcon: Icon(Icons.perm_contact_calendar,
                  color: Color.fromRGBO(0, 74, 162, 1)),
              suffixIcon: IconButton(
                onPressed: () {
                  function();
                },
                icon: Icon(Icons.search, color: Color.fromRGBO(0, 74, 162, 1)),
              ),
            )
        ),
      );

  var phone = (controller, placeholder, {f}) =>
      Container(
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(55),
            border: Border.all(
                color: Color.fromRGBO(218, 223, 232, 1),
                width: 1
            )
        ),
        child: TextField(
//            inputFormatters: [maskFormatter],
            keyboardType: TextInputType.phone,
            controller: controller,
            style: TextStyle(
                color: Color.fromRGBO(134, 135, 149, 1),
                fontSize: 16
            ),
            decoration: new InputDecoration(
              border: InputBorder.none,
              hintText: placeholder,
              prefixIcon: IconButton(
                onPressed: (){f();},
                icon: Icon(
                    Icons.perm_contact_calendar,
                    color: Color.fromRGBO(134, 135, 149, 1)
                ),
              ),
            )
        ),
      );
}