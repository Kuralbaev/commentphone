import 'package:commit360/utilites/styleLabel.dart';
import 'package:flutter/material.dart';

import 'loader.dart';

class ButtonWidget{
  final blue = (function, {text = '', disabled = false, load = false}) => MaterialButton(
    padding: EdgeInsets.all(0),
    onPressed: (){if(!disabled) function();},
    child: Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 74, 162, disabled ? 0.5 : 1),
        borderRadius: BorderRadius.circular(55),
      ),
      width: double.infinity,
      height: 50,
      child: Center(
        child: load ? CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ) : Text(text, style: labelButtonBlue()),
      ),
    ),
  );

  final white = (function, {text = '', disabled = false, textColor = 'white'}) => MaterialButton(
    padding: EdgeInsets.all(0),
    onPressed: (){if(!disabled) function();},
    child: Container(
      decoration: BoxDecoration(
        boxShadow: [
          new BoxShadow(
            color: Color.fromRGBO(31, 32, 65, 0.05),
            offset: new Offset(10, 20),
            blurRadius: 10,
          )
        ],
        color: Colors.white,
        borderRadius: BorderRadius.circular(55),
      ),
      width: double.infinity,
      height: 50,
      child: Center(
        child: Text(text, style: textColor == 'white' ? labelButtonWhite() : labelButtonWhiteOrange()),
      ),
    ),
  );
}