import 'package:commit360/screens/user/profile.dart';
import 'package:flutter/material.dart';

class AppBarWidget extends StatelessWidget{
  app({bool back = false, bool user = false}){
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.white,
      actions: <Widget>[
        IconButton(
          onPressed: (){},
          icon: user ? Builder(
              builder: (context) => IconButton(
                icon: Icon(Icons.person_outline, size: 26, color: Color.fromRGBO(107, 107, 107, 1),),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                },
              )
          ) : Container(),
        ),
        SizedBox(width: 10,)
      ],
      leading: back ? Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.arrow_back, size: 20, color: Color.fromRGBO(107, 107, 107, 1),),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
      ) : Container(),
      title: Image.asset('assets/images/logo.png'),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}