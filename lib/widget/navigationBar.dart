import 'package:commit360/screens/list/addNewCommit.dart';
import 'package:commit360/screens/other.dart';
import 'package:commit360/screens/searchByPhone.dart';
import 'package:commit360/utilites/animatedRoute.dart';
import 'package:commit360/utilites/styleLabel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class NavigatorBar extends StatefulWidget{
  int count;
  NavigatorBar(this.count);

  @override
  _NavBar createState() => _NavBar();
}

class _NavBar extends State<NavigatorBar>{
  int _selectedIndex = 0;

  _navigatorElement(width, text, count, icon){
    return MaterialButton(
      padding: EdgeInsets.all(0),
      onPressed: (){
        handlerNavigator(count);
      },
      child: Container(
        width: width / 3,
        child: Column(
          children: <Widget>[
            Icon(icon, color: _selectedIndex == count ? Color.fromRGBO(235, 137, 0, 1) : Color.fromRGBO(0, 74, 161, 1)),
            SizedBox(height: 5,),
            Text(text, style: _selectedIndex == count ? labelOrange(11) : labelBlue(11),),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    _selectedIndex = widget.count;

    return Container(
        width: double.infinity,
        height: 70,
        padding: EdgeInsets.symmetric(vertical: 10),
        color: Colors.white,
        child: Row(
          children: <Widget>[
            _navigatorElement(width, 'Поиск по номеру', 0, Icons.search),
            _navigatorElement(width, 'Новый отзыв', 1, Icons.add),
            _navigatorElement(width, 'Другое', 2, Icons.help_outline)
          ],
        )
    );
  }

  handlerNavigator(index){
    if (mounted) setState(() {
      _selectedIndex = index;
    });

    switch(index){
      case 0:{
        Navigator.push(context, MaterialPageRoute(builder: (context) => SearchByNumberPage()));
      }
      break;

      case 1:{
        Navigator.push(context, FadeRoute(page: ListAddNewCommit()));
      }
      break;

      case 2:{
        Navigator.push(context, FadeRoute(page: OtherPage()));
      }
      break;

      default: {

      }
      break;
    }
  }
}