import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import 'loader.dart';

class AlertWidget extends StatelessWidget{
  Widget show(BuildContext context, text, type) {
    return Flushbar(
      message: text,
      backgroundColor: type == 'success' ? Color.fromRGBO(39, 174, 96, 1) : Color.fromRGBO(235, 87, 87, 1),
      duration:  Duration(seconds: 3),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}