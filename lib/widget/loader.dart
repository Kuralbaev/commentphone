import 'package:flutter/material.dart';

class LoaderWidget {
  var list = (text) => SingleChildScrollView(
    child: Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
      ),
      width: 50,
      child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 5,),
            CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Color.fromRGBO(0, 71, 76, 1)),
            ),
            SizedBox(height: 15,),
            Text(text,textAlign: TextAlign.center,)
          ],
        ),
      ),
    ),
  );
}