import 'package:commit360/utilites/styleCards.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

var _name = (text) => Container(
      width: 300,
      child: Text(
          text,
          style: TextStyle(
              color: Color.fromRGBO(134, 135, 149, 1),
              fontSize: 18,
              fontWeight: FontWeight.bold
          )),
    );

var _phone = (text) => Text(
    text,
    style: TextStyle(
        color: Color.fromRGBO(134, 135, 149, 1),
        fontSize: 14,
        fontWeight: FontWeight.w300
    ));

var _myCommitTitle = (text) => Text(
      text,
      style: TextStyle(
          color: Color.fromRGBO(107, 107, 107, 1),
          fontSize: 16,
          fontWeight: FontWeight.w700
      ));

var _myCommitNote = (text) => Text(
      text,
      style: TextStyle(
          color: Color.fromRGBO(107, 107, 107, 1),
          fontSize: 16,
          fontWeight: FontWeight.w300
      ));

class CardWidget{
  add (data, function, status) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color.fromRGBO(218, 223, 232, 1)))
      ),
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: 230,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _name(data['name']),
                SizedBox(height: 5,),
                _phone(data['phone'])
              ],
            ),
          ),
          Container(
            width: 60,
            height: 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: status > -1 ? Color.fromRGBO(218, 223, 232, 1) : Color.fromRGBO(39, 174, 96, 1)),
              color: status > -1 ? Colors.white : Color.fromRGBO(39, 174, 96, 1),
            ),
            child: Center(
              child: MaterialButton(
                onPressed: (){
                  if(status == -1) function(data['name'],data['phone']);
                },
                padding: EdgeInsets.all(0),
                child: status > -1 ? Icon(Icons.check, color: Color.fromRGBO(218, 223, 232, 1)) : Icon(Icons.add, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }

  byPhone (data, function, status) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Color.fromRGBO(218, 223, 232, 1)))
      ),
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: 230,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _name(data['name']),
                SizedBox(height: 5,),
                _phone(data['phone'])
              ],
            ),
          ),

          Container(
            width: 60,
            height: 30,
            child: Center(
              child: MaterialButton(
                onPressed: (){
                  function(data['name'],data['phone']);
                },
                padding: EdgeInsets.all(0),
                child: Icon(Icons.arrow_forward, color: Colors.green),
              ),
            ),
          )
        ],
      ),
    );
  }

  myCommit(data, update, remove){
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(15),
          decoration: cardStyle(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 230,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _name(data['name'] ?? 'No name'),
                        SizedBox(height: 5,),
                        _phone(data['phone'] ?? '')
                      ],
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border.all(width: 1, color: Color.fromRGBO(45, 156, 219, 1))
                        ),
                        child: Center(
                          child: IconButton(
                            onPressed: (){
                              update(data['id'], data['name'] ?? '', data['phone']);
                            },
                            icon: Icon(Icons.edit, size: 18, color: Color.fromRGBO(45, 156, 219, 1)),
                          ),
                        ),
                      ),
//                      SizedBox(width: 10),
//                      Container(
//                        width: 35,
//                        height: 35,
//                        decoration: BoxDecoration(
//                            borderRadius: BorderRadius.circular(50),
//                            border: Border.all(width: 1, color: Color.fromRGBO(235, 87, 87, 1))
//                        ),
//                        child: Center(
//                          child: IconButton(
//                            onPressed: (){
//                              remove(data['id']);
//                            },
//                            icon: Icon(Icons.delete_forever, size: 18, color: Color.fromRGBO(235, 87, 87, 1)),
//                          ),
//                        ),
//                      ),
                    ],
                  )
                ],
              ),
              SizedBox(height: 20,),
              _myCommitTitle('Положительные качества'),
              SizedBox(height: 5,),
              _myCommitNote(data['good_review'] ?? '...'),
              SizedBox(height: 10),

              _myCommitTitle('Отрицательные качества'),
              SizedBox(height: 5,),
              _myCommitNote(data['bad_review'] ?? '...'),SizedBox(height: 10),

              _myCommitTitle('Комментарий'),
              SizedBox(height: 5,),
              _myCommitNote(data['comment'] ?? '...'),SizedBox(height: 10),

              Row(
                children: <Widget>[
                  Icon(Icons.calendar_today, size: 12, color: Color.fromRGBO(107, 107, 107, 1),),
                  SizedBox(width: 5,),
                  Text('01.01.2020', style: TextStyle(
                      color: Color.fromRGBO(107, 107, 107, 1),
                      fontSize: 11
                  ),)
                ],
              )
            ],
          ),
        ),
        SizedBox(height: 15)
      ],
    );
  }

  commit(data){
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(15),
          decoration: cardStyle(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 20,),
              _myCommitTitle('Положительные качества'),
              SizedBox(height: 5,),
              _myCommitNote(data['good_review'] ?? '...'),
              SizedBox(height: 10),

              _myCommitTitle('Отрицательные качества'),
              SizedBox(height: 5,),
              _myCommitNote(data['bad_review'] ?? '...'),SizedBox(height: 10),

              _myCommitTitle('Комментарий'),
              SizedBox(height: 5,),
              _myCommitNote(data['comment'] ?? '...'),SizedBox(height: 10),
              SizedBox(height: 15),

              Row(
                children: <Widget>[
                  Icon(Icons.calendar_today, size: 12, color: Color.fromRGBO(107, 107, 107, 1),),
                  SizedBox(width: 5,),
                  Text(DateFormat('dd.MM.yyyy').format(DateTime.parse(data['created_at'])).toString(), style: TextStyle(
                      color: Color.fromRGBO(107, 107, 107, 1),
                      fontSize: 11
                  ),)
                ],
              )
            ],
          ),
        ),
        SizedBox(height: 15,)
      ],
    );
  }
}